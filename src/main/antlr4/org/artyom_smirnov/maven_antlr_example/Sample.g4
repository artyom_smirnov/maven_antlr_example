grammar Sample;

prog:	(expr NEWLINE)* ;
expr:	expr ('*'|'/') expr # MUL_DIV
    |	expr ('+'|'-') expr # PLUS_MINUS
    |	INT                 # INT
    |	'(' expr ')'        # PARENS
    |   function            # FUNCTION_CALL
    ;

function:
    IDENTIFIER '(' expr (',' expr)* ')'
    ;

IDENTIFIER: [a-z][a-z0-9_]+ ;
NEWLINE : [\r\n]+ ;
INT     : [0-9]+ ;