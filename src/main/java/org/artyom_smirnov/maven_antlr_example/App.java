package org.artyom_smirnov.maven_antlr_example;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

public class App
{
    public static void main( String[] args )
    {
        try {
            ANTLRInputStream reader = new ANTLRInputStream("abc(1+1,2,3)");
            SampleLexer lexer  = new SampleLexer(reader);
            TokenStream tokens = new CommonTokenStream(lexer);
            SampleParser parser = new SampleParser(tokens);
            ParseTree tree = parser.expr();
            ExpressionVisitor visitor = new ExpressionVisitor();
            visitor.visit(tree);
        } catch (Exception e) {
            System.out.printf("Parsing error: %s\n", e);
        }
    }
}
