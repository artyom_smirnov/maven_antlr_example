package org.artyom_smirnov.maven_antlr_example;

public class ExpressionVisitor extends SampleBaseVisitor {
    @Override
    public Object visitFunction(SampleParser.FunctionContext ctx) {
        for (int i = 0; i < ctx.getChildCount(); i++)
        {
            System.out.println(ctx.getChild(i).getText());
        }
        return super.visitFunction(ctx);
    }
}
